use serde_json;
use ipnet::{IpNet, NetworkDeviceInfo};
fn main() {
    let ip = IpNet::new();
    println!("{}", serde_json::to_string(ip.devices()).unwrap());
}
