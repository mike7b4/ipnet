use std::process::Command;
use serde::Serialize;
use std::net::{Ipv4Addr, Ipv6Addr};
use std::collections::HashMap;
use std::iter::Iterator;
use regex::{Regex};
use regex::Error as RegexError;

#[derive(Debug, PartialEq, Serialize)]
pub enum NetworkState {
    Up,
    Down,
    Unknown
}

#[derive(Debug, Serialize)]
pub struct NetworkDeviceInfo {
    pub state: NetworkState,
    pub ipv4: Option<Ipv4Addr>,
    pub ipv6: Option<Ipv6Addr>,
    pub mac: String,
    pub mtu: u16
}

impl NetworkDeviceInfo {
    fn new() -> NetworkDeviceInfo {
        NetworkDeviceInfo {
            state: NetworkState::Unknown,
            ipv4: None,
            ipv6: None,
            mac: "".to_string(),
            mtu: 0
        }
    }
}

pub struct IpCommand {
    devices: HashMap<String, Vec<String>>
}

impl IpCommand {
    pub fn new(subcommand: &str) -> Result<IpCommand, std::io::Error> {
        let out = Command::new("ip").arg(subcommand).output()?;
        let mut ipcommand = IpCommand { devices : HashMap::new() };
        let head = Regex::new(r"^\d: [a-z0-9]:*").unwrap();
        let mut device = "".to_string();
        let lines = String::from_utf8_lossy(&out.stdout);
        let lines = lines.lines();
        match subcommand {
            "address" | "link" => {
                let mut dev_lines = vec!();
                for line in lines {
                    // Got a new device?
                    if head.is_match(line) {
                        let dev = Regex::new(r"^([0-9]+): ([a-z0-9]+)").unwrap().captures(&line);
                        if dev.is_some() {
                            if &device.len() > &0 {
                                ipcommand.devices.insert(device, dev_lines);
                            }

                            device = dev.unwrap()[0][3..].to_string();
                            dev_lines = vec!();
                        }
                    }
                    dev_lines.push(line.clone().to_string()); //line.to_string());
                }
                /// store last
                ipcommand.devices.insert(device, dev_lines);
            },
            _ => { eprintln!("Parser not implemented for {}", &subcommand); }
        };

        Ok(ipcommand)
    }
}

pub struct IpNet {
    devices: HashMap<String, NetworkDeviceInfo>
}

impl IpNet {
    pub fn new() -> IpNet {
        let mut ip = IpNet {
            devices: HashMap::new()
        };

        #[cfg(not(test))]
        ip.refresh().unwrap();
        ip
    }

    pub fn refresh(&mut self) -> std::result::Result<(), std::io::Error> {
        let ip = IpCommand::new("address")?;

        self.devices = HashMap::new();
        for (id, device) in ip.devices {
            self.deserialize_device(id, device).unwrap();
        }
        Ok(())
    }

    fn deserialize_device(&mut self, id: String, device: Vec<String>) -> Result<(), RegexError> {
        if device.is_empty() || id.is_empty() {
            return Err(RegexError::Syntax("".to_string()));
        }
        let mut net = NetworkDeviceInfo::new();
        let rs = Regex::new(r"^\d: [a-z0-9]:*")?;
        let head = match device.iter().next() {
            Some(head) => {
                if rs.is_match(head) == false {
                    return Err(RegexError::Syntax("".to_string()));
                }
                head
            },
            None => return Err(RegexError::Syntax("".to_string()))
        };
        let mtu = Regex::new(r"(mtu [0-9]+)")?.captures(&head);
        let state = Regex::new(r"(state [A-Z]+)")?.captures(&head);
        if mtu.is_some() {
            net.mtu = mtu.unwrap()[0][4..].parse::<u16>().unwrap_or(0);
        }

        if state.is_some() {
            net.state = match &state.unwrap()[0] {
                "state DOWN" => NetworkState::Down,
                "state UP" => NetworkState::Up,
                _ => NetworkState::Unknown,
            };
        }
        let inet = Regex::new(r"(inet )([0-9]+)(.)([0-9]+)(.)([0-9]+)(.)([0-9]+)")?;
        let inet6 = Regex::new(r"^[\s]+(inet6 )")?;
        let link = Regex::new(r"^[\s]+(link/ether )([0-9a-f]{2}:){5}")?;
        for line in device {
            if inet.is_match(&line) {
                let inet = Regex::new(r"inet (([0-9]+)(.)([0-9]+)(.)([0-9]+)(.)([0-9]+))").
                    unwrap().
                    captures(&line);

                if inet.is_some() {
                    let ip = inet.unwrap()[1].parse::<Ipv4Addr>();
                    
                    //let addr: SocketAddr = inet.unwrap_or(["",""])[1].parse();
                    net.ipv4 = match ip {
                        Ok(addr) => Some(addr),
                        Err(_) => None
                    };
                }
            }
            else if inet6.is_match(&line) {
            }
            else if link.is_match(&line) {
                let ether = Regex::new(r"(/ether )([0-9a-f]{2}:){5}([0-9a-f]{2})")?.captures(&line);
                if ether.is_some() {
                    net.mac = ether.unwrap()[0][7..].to_string();
                }
            }
        }
 
        self.devices.insert(id, net);
        Ok(())
    }

    pub fn devices(&self) -> &HashMap<String, NetworkDeviceInfo> {
        &self.devices
    }
}

mod tests {
    use crate::ipnet::{IpNet, NetworkState};
    use std::net::{Ipv4Addr};
    #[test]
    fn deserialize_fail_test() {
        let mut ip = IpNet::new();
        let lines: Vec<String> = Vec::new();
        assert!(ip.deserialize_device("apa".to_string(), lines).is_err(), "Deserialize should fail");
        assert!(ip.devices.is_empty(), true);
        let mut lines: Vec<String> = Vec::new();
        lines.push("3: wlp2s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000".to_string());
        assert!(ip.deserialize_device("".to_string(), lines).is_err());
        assert!(ip.devices.is_empty());
    }


    #[test]
    fn deserialize_without_ip_field() {
        let mut ip = IpNet::new();
        let mut lines: Vec<String> = Vec::new();
        lines.push("3: wlp2s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000".to_string());
        assert!(ip.deserialize_device("wlp2s0".to_string(), lines).is_ok());
        assert!(ip.devices.is_empty() == false);
        assert!(ip.devices["wlp2s0"].mtu == 1500);
        assert!(ip.devices["wlp2s0"].state == NetworkState::Down);
        assert!(ip.devices["wlp2s0"].ipv4 == None);
    }

    #[test]
    fn deserialize_with_ip_field() {
        let mut ip = IpNet::new();
        let mut lines: Vec<String> = Vec::new();
        lines.push("3: wlp2s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state UP group default qlen 1000".to_string());
        lines.push("    inet 10.7.7.4/24 brd 10.7.7.255 scope global noprefixroute tun0".to_string());
        assert!(ip.deserialize_device("wlp2s0".to_string(), lines).is_ok());
        assert!(ip.devices.is_empty() == false);
        assert!(ip.devices["wlp2s0"].mtu == 1500);
        assert!(ip.devices["wlp2s0"].state == NetworkState::Up);
        assert!(ip.devices["wlp2s0"].ipv4 == Some(Ipv4Addr::new(10, 7, 7, 4)));
      }

}
